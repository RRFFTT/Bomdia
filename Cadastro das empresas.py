

from ferramentas import menus

#menus.excluir()
#menus.mensagem()
#menus.verclientescadastrados()
#menus.converter('Nomes clientes', ler=True)
#menus.converter('Nomes clientes', att=True)
#menus.cadastrocliente()




while True:
        print(' MENU '.center(100, "█"))
        print(f'\nEscolha uma opção:\n'
              f'1 - Ver clientes cadastrados\n'
              f'2 - Visualizar mensagem de Bom Dia\n'
              f'3 - Adicionar um novo cliente\n'
              f'4 - Excluir cliente\n'
              f'5 - Sair\n')
        print('='*100)
        opcao = int(input('Qual opção deseja escolher: '))
        print('=' * 100)
        if opcao > 5 or opcao == 0:
            print('Opção Inválida.Tente novamente.')
        if opcao == 1:
            menus.verclientescadastrados()
        if opcao == 2:
            while True:
                menus.mensagem()
                continua = str(input('Quer continuar [S ou N]: ').upper())
                if continua == 'N':
                    break
        if opcao == 3:
                menus.cadastrocliente()
        if opcao == 4:
            menus.excluir()
        if opcao == 5:
            print(' FIM '.center(100, '='))
            break
