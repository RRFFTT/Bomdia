cliente = list()


def converter(a, ler=0, att=0):

    if ler:
        arquivo = open(f'{a}.txt', 'r')
        for nome in arquivo:
            cliente.append(nome.strip().title())
        arquivo.close()
        cliente.sort()
    if att:
        cliente.sort()
        arquivo = open(f'{a}.txt', 'w')
        for nome in cliente:
            if
            arquivo.write(nome + '\n')
        arquivo.close()


def titulo(titulo):
    print('|', f'{"_"}' * 55, '|')
    print(f'|{f"{titulo.upper()}":^57}|')
    print('|', f'{"_"}' * 55, '|')
    print(f'|{" ":^57}|')


def verclientescadastrados():
    titulo('Clientes cadastrados')
    converter('Nomes clientes', ler=True)
    for num, nome in enumerate(cliente):
        if num < 9:
            num = f'0{num + 1}'
            print(f'| {num:^5} {nome:<50}|')
        else:
            print(f'| {num + 1:^5} {nome:<50}|')
    print('|', f'{"_"}' * 55, '|')
    cliente.clear()



def cadastrocliente():
    titulo('cadastro cliente')
    converter('Nomes clientes', ler=True)
    add_novo = str(input('Nome do novo cliente: ').title())
    if add_novo in cliente:
        print('Cliente já cadastrado')
    else:
        converter('Nomes clientes', ler=True)
        cliente.append(add_novo)
        cliente.sort()
        for c in cliente:
            if
        print(f'{"CLIENTE ADICIONADO":^57}')
        cliente.clear()




def mensagem():

    resultado_busca = list()

    #                          GERADOR DA FRASE DE BOM DIA
    # ▼codigo para passar o arquivo txt em uma lista, para usar na formulação da frase▼
    arquivoclientes = open('Nomes clientes.txt', 'r')
    for cod, cliente_cadastrado in enumerate(arquivoclientes):
        if cod < 9:
            cliente.append(f'0{cod + 1}: {cliente_cadastrado}'.strip().title())
        else:
            cliente.append(f'{cod + 1}: {cliente_cadastrado}'.strip().title())
    arquivoclientes.close()
    # ▲ codigo para passar o arquivo txt em uma lista, para usar na formulação da frase ▲
    escolha = str(input('Para qual cliente deseja visualizar a mensagem de bom dia > ').lower())
    for nome in cliente:
        nome = nome.lower()
        if escolha in nome:
            resultado_busca.append(nome.title())
    resultados = len(resultado_busca)
    if resultados == 1:
        cliente_selecionado = resultado_busca[0]
        if cliente_selecionado.find('Grupo') > 0:
            print(f' {cliente_selecionado[4:]}'.upper().center(90))
            print(f'Bom dia, tudo bem? \nPassando para desejar a todos do {cliente_selecionado[4:]} uma excelente e '
                  f'prospera semana.'
                  f'\nEstamos a disposição para esclarecer e auxiliar em qualquer dúvida que venham a ter.'
                  f'\nAtenciosamente Rafael Reffatti; \nAgente de suporte Sirius.')
        else:
            print(f' {cliente_selecionado[4:].upper().center(90)}')
            print(f'Bom dia, tudo bem? \nPassando para desejar a todos da {cliente_selecionado[4:]} uma excelente e '
                  f'prospera semana.'
                  f'\nEstamos a disposição para esclarecer e auxiliar em qualquer duvida que venham a ter.'
                  f'\nAtenciosamente Rafael Reffatti; \nAgente de suporte Sirius.')
    if resultados > 1:
        try:
            print(f'Foram encontardos {len(resultado_busca)} clientes com {escolha}.')
            for encontrado in resultado_busca:
                print(encontrado)
            selecionar = int(input('Digite o codigo que deseja selecionar: '))
            cliente_selecionado = cliente[selecionar - 1]
            if cliente_selecionado.find('Grupo') > 0:
                print(f' {cliente_selecionado[4:]}'.upper().center(90))
                print(
                    f'Bom dia, tudo bem? \nPassando para desejar a todos do {cliente_selecionado[4:]} uma excelente e prospera semana.'
                    f'\nEstamos a disposição para esclarecer e auxiliar em qualquer duvida que venham a ter.'
                    f'\nAtenciosamente Rafael Reffatti; \nAgente de suporte Sirius.')
            else:
                print(f' {cliente_selecionado[4:].upper().center(90)}')
                print(
                    f'Bom dia, tudo bem? \nPassando para desejar a todos da {cliente_selecionado[4:]} uma excelente e '
                    f'prospera semana.'
                    f'\nEstamos a disposição para esclarecer e auxiliar em qualquer duvida que venham a ter.'
                    f'\nAtenciosamente Rafael Reffatti; \nAgente de suporte Sirius.')
        except:
            print('Opção inválida!!! Tente novamente')
    if resultados == 0:
        print(f'A busca resultou em {resultados} clientes.')
    resultado_busca.clear()


def excluir():
    cliente = list()
    busca = list()

    converter('Nomes clientes', ler=True)
    selecionar = str(input('Qual empresa deseja excluir ? ').lower())
    for nome in cliente:
        nome_lower = nome.lower()
        if selecionar in nome_lower:
            busca.append(nome)
    qnt_resultados = len(busca)
    if qnt_resultados == 0 or qnt_resultados == 1:
        if qnt_resultados == 1:
            print(f'Foi encontrado o cliente {busca[0]}')
            confirma = str(input(f'Deseja confirmar a remoção de [ {busca[0].upper()} ]: '))
            if confirma.upper() == 'S':
                cliente.remove(f'{busca[0].title()}')
                converter('Nomes clientes', att=True)
                titulo('Cliente excluido')
            elif confirma.upper() == 'N':
                print('Operação cancelada.')
            elif confirma.upper() != 'S' or 'N':
                print('Opção inválida, tente novamente !!')
        elif qnt_resultados == 0:
            print('A busca resultou em 0 clientes encontrados !! Tente novamente.')
    else:
        if qnt_resultados < 10:
            print(f'A busca resultou em 0{qnt_resultados} resultados:')
        else:
            print(f'A busca resultou em 0{qnt_resultados} resultados:')
        for num, resultado in enumerate(busca):
            print(f'Opção [ {num + 1} ]: {resultado}')
        selecionar_resultado = str(input('Qual cliente deseja selecionar para excluir?  '))
        try:
            selecionado = busca[int(selecionar_resultado) - 1]
            confirma = str(input(f'Confirmar exclusão de → {selecionado} ← [ S/N ] ? ').upper())
            if confirma == 'S' or confirma == 'N':
                if confirma == 'S':
                    cliente.remove(f'{selecionado}')
                else:
                    print('Operação cancelada !! ')
            else:
                print('Opção inválida !!')
            for nome in cliente:
                print(nome)
        except:
            print('Opção não encotrada !! ')



